# MacPaw test project

Test project for internship of Back-End PHP Engineering


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/shumerok/macpaw_test.git macpaw_test
```

Go to the project directory

```bash
  cd macpaw_test
```

## Deployment
To deploy this project run:

```bash
docker-compose up -d --build
```
```bash
docker exec -it macpaw_php composer install
```

Copy the .env.example file to .env in the root folder:
```bash
docker exec -it macpaw_php cp .env.example .env
```

#### Then run:

```bash 
docker exec -it macpaw_php php artisan key:generate
```

```bash
docker exec -it macpaw_php php artisan migrate
```
####

To  request  data for the last 3 days from nasa api (https://api.nasa.gov/ -> API Name Asteroids - NeoWs) and insert into the database use the following command:
```bash 
docker exec -it macpaw_php php artisan db:seed
```

### Go to http://localhost:8876/
