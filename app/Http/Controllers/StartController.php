<?php

namespace App\Http\Controllers;

class StartController extends Controller
{
    public function index()
    {
        $startingWords = ['hello' => 'MacPaw Internship 2022!'];

        return view('macpaw',compact('startingWords'));
    }
}
