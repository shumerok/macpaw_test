<?php

namespace App\Http\Controllers;

use App\Services\NearEarthObject\ShowService;
use Illuminate\Http\Request;

class NearEarthObjectController extends Controller
{
    public object $service;

    public function __construct(ShowService $service)
    {
        $this->service = $service;
    }

    public function show(Request $request)
    {
        $defaultHazardous = $request->query('hazardous', 'false');

        $fastest = $this->service->show($defaultHazardous);

        return view('fastest', ['fastest' => $fastest]);
    }

    public function hazardous()
    {
        $hazard = $this->service->hazardous();

        return view('hazardous', compact('hazard'));
    }

}

