<?php

declare(strict_types=1);

namespace App\Interfaces;

use Illuminate\Database\Eloquent\Collection;

interface NearEarthObjectRepositoryInterface
{
    public function getFastHazardAsteroids(): Collection;

    public function getHazardousAsteroids(): Collection;

    public function getFastestAsteroids(): Collection;
}

