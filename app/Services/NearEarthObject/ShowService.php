<?php

declare(strict_types=1);

namespace App\Services\NearEarthObject;

use App\Interfaces\NearEarthObjectRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ShowService
{
    private NearEarthObjectRepositoryInterface $nearEarthObjectRepository;

    public function __construct(NearEarthObjectRepositoryInterface $nearEarthObjectRepository)
    {
        $this->nearEarthObjectRepository = $nearEarthObjectRepository;
    }

    public function show($defaultHazardous): array|Collection
    {
        if ($defaultHazardous === 'true') {
            $fastest = $this->nearEarthObjectRepository->getFastHazardAsteroids();
            return $this->emptyHazardous($fastest);
        }

        return $this->nearEarthObjectRepository->getFastestAsteroids();
    }

    public function hazardous(): array|Collection
    {
        $hazard = $this->nearEarthObjectRepository->getHazardousAsteroids();
        return $this->emptyHazardous($hazard);
    }

    private function emptyHazardous(Collection $hazard): array|Collection
    {
        if ($hazard->isEmpty()) {
            return ["There is no hazardous asteroids"];
        }

        return $hazard;

    }
}


