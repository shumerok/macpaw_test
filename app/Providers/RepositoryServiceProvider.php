<?php

namespace App\Providers;

use App\Interfaces\NearEarthObjectRepositoryInterface;
use App\Repositories\NearEarthObjectRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(NearEarthObjectRepositoryInterface::class, NearEarthObjectRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
