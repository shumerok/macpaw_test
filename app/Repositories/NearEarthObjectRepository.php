<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Interfaces\NearEarthObjectRepositoryInterface;
use App\Models\NearEarthObject;
use Illuminate\Database\Eloquent\Collection;

class NearEarthObjectRepository implements NearEarthObjectRepositoryInterface
{
    public function getFastHazardAsteroids(): Collection
    {
        return NearEarthObject::where('is hazardous', 1)->orderBy('speed', 'desc')->get();
    }

    public function getHazardousAsteroids(): Collection
    {
        return NearEarthObject::where('is hazardous', 1)->get();
    }

    public function getFastestAsteroids(): Collection
    {
        return NearEarthObject::orderBy('speed', 'desc')->get();
    }

}

