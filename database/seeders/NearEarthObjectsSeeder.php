<?php

namespace Database\Seeders;

use App\Models\NearEarthObject;
use Illuminate\Database\Seeder;

class NearEarthObjectsSeeder extends Seeder
{
    private string $apiKey;
    private string $nowDate;
    private string $threeDayAgo;
    private array $formattedArray;

    public function __construct()
    {

        $this->nowDate = date('Y-m-d');
        $this->threeDayAgo = date('Y-m-d', strtotime('-3 day'));
        $this->apiKey = 'lNQ7Btrf6e1BqTkiop9xT2KhUHmEwyp6sWSlfH8J';
        $this->formattedArray = [];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $asteroids = $this->JsonToArray();

        foreach ($asteroids as $value) {
            NearEarthObject::create([
                'referenced' => $value['neo_reference_id'],
                'name' => $value['name'],
                'speed' => $value['kilometers_per_hour'],
                'is hazardous' => $value['is_potentially_hazardous_asteroid'],
                'Date' => $value['close_approach_date'],
            ]);
        }
    }

    private function JsonToArray(): array
    {
        $jsonNasa = file_get_contents("https://api.nasa.gov/neo/rest/v1/feed?"
            . "start_date=$this->threeDayAgo"
            . "&end_date=$this->nowDate"
            . "&api_key=$this->apiKey"
        );

        $decodeJsonNasa = json_decode($jsonNasa, true);
        $elements = $decodeJsonNasa['near_earth_objects'];
        $asteroids = [];

        foreach ($elements as $value) {
            foreach ($value as $element) {
                $asteroids[] = self::keyValueExtractor($element);
            }
        }

        return $asteroids;
    }

    private function keyValueExtractor($json): array
    {

        foreach ($json as $key => $value) {
            if (is_array($value)) {
                self::keyValueExtractor($value);
            } else {
                if (empty($value)) {
                    $value = 0;
                    $this->formattedArray[$key] = $value;
                }
                $this->formattedArray[$key] = $value;
            }
        }

        return $this->formattedArray;
    }
}
