<?php

namespace Tests\Feature;

use Tests\TestCase;

class ProjectTest extends TestCase
{
    public function testIndex()
    {
        $response = $this->get('/');
        $response->assertOk();
    }

    public function testHazardous()
    {
        $response = $this->get('/neo/hazardous');
        $response->assertOk();
    }

    public function testFastest()
    {
        $response = $this->get('/neo/fastest');
        $response->assertOk();
    }
}
