<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>{{ json_encode($startingWords) }}</h1>
<ul>Routes:
    <li><a href="http://localhost:8876/neo/hazardous">/neo/hazardous</a></li>
    <li><a href="http://localhost:8876/neo/fastest">/neo/fastest</a></li>
    <li><a href="http://localhost:8876/neo/fastest?hazardous=true">/fastest?hazardous=true</a></li>
    <li><a href="http://localhost:8876/neo/fastest?hazardous=false">/fastest?hazardous=false</a></li>
</ul>
</body>
</html>

